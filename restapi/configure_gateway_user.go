// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"context"
	"crypto/tls"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"google.golang.org/grpc"

	pb "gwusermod/api"
	"gwusermod/restapi/operations"
	"gwusermod/restapi/operations/auth"
	"gwusermod/restapi/operations/movies"
	"gwusermod/restapi/operations/profile"
	"gwusermod/restapi/service"
)

//go:generate swagger generate server --target ../../gateway-user --name GatewayUser --spec ../api/swagger.yaml --principal interface{}

func configureFlags(api *operations.GatewayUserAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.GatewayUserAPI) http.Handler {

	ctx, _ := context.WithTimeout(context.Background(), 10 * time.Second)

	// configure the api here
	log.Printf("Configuring gateway-user API")
	api.ServeError = errors.ServeError

	authServiceAddr := os.Getenv("AUTH_SERVICE_GRPC_ADDR")
	if authServiceAddr == "" {
		log.Fatal("Unable to retrieve env AUTH_SERVICE_GRPC_ADDR")
	}

	//userServiceAddr := os.Getenv("USER_SERVICE_GRPC_ADDR")
	//if userServiceAddr == "" {
	//	log.Fatal("Unable to retrieve env USER_SERVICE_GRPC_ADDR")
	//}

	moviesServiceAddr := os.Getenv("MOVIES_SERVICE_GRPC_ADDR")
	if moviesServiceAddr == "" {
		log.Fatal("Unable to retrieve env USER_SERVICE_GRPC_ADDR")
	}

	//authConn, err := grpc.Dial("auth-service:"+authServiceAddr, grpc.WithInsecure(), grpc.WithBlock())
	//authConn, err := grpc.Dial("0.0.0.0:"+authServiceAddr, grpc.WithInsecure(), grpc.WithBlock())
	authConn, err := grpc.DialContext(ctx, "auth-service:"+authServiceAddr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("Can't connect to auth service. Cause: %v", err)
	}
	//defer authConn.Close()
	cAuth := pb.NewAuthServiceClient(authConn)
	log.Printf("GRPC dial to Auth service is OK")

	//moviesConn, err := grpc.Dial("movies-service:"+moviesServiceAddr, grpc.WithInsecure(), grpc.WithBlock())
	//moviesConn, err := grpc.Dial("0.0.0.0:"+moviesServiceAddr, grpc.WithInsecure(), grpc.WithBlock())
	moviesConn, err := grpc.DialContext(ctx, "movies-service:"+moviesServiceAddr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("Can't connect to Movies service. Cause: %v", err)
	}
	log.Printf("GRPC dial to Movies service is OK")
	//defer moviesConn.Close()
	cMov := pb.NewMovieServiceClient(moviesConn)

	//userConn, err := grpc.Dial("0.0.0.0:"+userServiceAddr, grpc.WithInsecure(), grpc.WithBlock())
	//if err != nil {
	//	log.Fatalf("Can't connect to User service. Cause: %v", err)
	//}
	//log.Printf("GRPC dial to User service is OK")
	//cUser := pb.NewUserServiceClient(userConn)

	s := &service.GatewayUserService{
		AuthServiceClient: cAuth,
		//UserServiceClient: cUser,
		MoviesServiceClient: cMov,
	}

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	// api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	// Applies when the "Authorization" header is set
	if api.BearerAuthAuth == nil {
		api.BearerAuthAuth = func(token string) (interface{}, error) {
			return nil, errors.NotImplemented("api key auth (bearerAuth) Authorization from header param [Authorization] has not yet been implemented")
		}
	}

	// Set your custom authorizer if needed. Default one is security.Authorized()
	// Expected interface runtime.Authorizer
	//
	// Example:
	// api.APIAuthorizer = security.Authorized()
	if api.MoviesGetMovieHandler == nil {
		api.MoviesGetMovieHandler = movies.GetMovieHandlerFunc(func(params movies.GetMovieParams) middleware.Responder {
			return middleware.NotImplemented("operation movies.GetMovie has not yet been implemented")
		})
	}
	if api.ProfileGetProfileHandler == nil {
		api.ProfileGetProfileHandler = profile.GetProfileHandlerFunc(func(params profile.GetProfileParams, principal interface{}) middleware.Responder {
			return middleware.NotImplemented("operation profile.GetProfile has not yet been implemented")
		})
	}

	api.MoviesListMoviesHandler = movies.ListMoviesHandlerFunc(s.ListMovies)

	if api.ProfileListOrdersHandler == nil {
		api.ProfileListOrdersHandler = profile.ListOrdersHandlerFunc(func(params profile.ListOrdersParams, principal interface{}) middleware.Responder {
			return middleware.NotImplemented("operation profile.ListOrders has not yet been implemented")
		})
	}
	if api.ProfileListPaymentsHandler == nil {
		api.ProfileListPaymentsHandler = profile.ListPaymentsHandlerFunc(func(params profile.ListPaymentsParams, principal interface{}) middleware.Responder {
			return middleware.NotImplemented("operation profile.ListPayments has not yet been implemented")
		})
	}

	api.AuthLoginUserHandler = auth.LoginUserHandlerFunc(s.Login)

	api.AuthRegisterUserHandler = auth.RegisterUserHandlerFunc(s.Register)

	if api.MoviesRentMovieHandler == nil {
		api.MoviesRentMovieHandler = movies.RentMovieHandlerFunc(func(params movies.RentMovieParams, principal interface{}) middleware.Responder {
			return middleware.NotImplemented("operation movies.RentMovie has not yet been implemented")
		})
	}

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
