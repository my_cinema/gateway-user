package service

import (
	"context"
	"encoding/json"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"github.com/golang/protobuf/ptypes"
	"google.golang.org/protobuf/types/known/timestamppb"
	pb "gwusermod/api"
	"gwusermod/models"
	"gwusermod/restapi/operations/movies"
	"log"
	"net/http"
	"time"
)

func (s *GatewayUserService) ListMovies(params movies.ListMoviesParams) (response middleware.Responder) {
	resp, err := s.listMovies(context.Background(), params)
	if err != nil {
		log.Println(err)
		return middleware.ResponderFunc(func(rw http.ResponseWriter, pr runtime.Producer) {
			rw.WriteHeader(500)
		})
	}
	bytes, err := json.Marshal(resp)
	if err != nil {
		log.Println(err)
		return middleware.ResponderFunc(func(rw http.ResponseWriter, pr runtime.Producer) {
			rw.WriteHeader(500)
		})
	}
	return middleware.ResponderFunc(func(rw http.ResponseWriter, pr runtime.Producer) {
		rw.WriteHeader(200)
		rw.Write(bytes)
	})
}

func (s *GatewayUserService) listMovies(ctx context.Context, params movies.ListMoviesParams) (*models.ListMoviesResponse, error) {
	var offset int32
	var onlyRented bool
	var mpa pb.MovieMPA
	var category pb.MovieCategory
	var yearStart *timestamppb.Timestamp
	var yearEnd *timestamppb.Timestamp

	if params.Offset != nil {
		offset = *params.Offset
	} else {
		offset = 0
	}

	if params.Mpa == nil {
		mpa = pb.MovieMPA_MovieMPA_NOTSET} else {
		switch mpa {
		case 0:
			mpa = pb.MovieMPA_MovieMPA_G
		case 1:
			mpa = pb.MovieMPA_MovieMPA_PG
		case 2:
			mpa = pb.MovieMPA_MovieMPA_PG
		case 3:
			mpa = pb.MovieMPA_MovieMPA_R
		case 4:
			mpa = pb.MovieMPA_MovieMPA_R
		}}

	if params.OnlyRented != nil {
		onlyRented = *params.OnlyRented
	} else {
		onlyRented = false
	}

	if params.Category == nil {
		category = pb.MovieCategory_MovieCategory_NOTSET
	} else {
	switch category {
	case 0:
		category = pb.MovieCategory_MovieCategory_ACTION
	case 1:
		category = pb.MovieCategory_MovieCategory_HORROR
	}}

	if params.YearStart != nil {
		yearStart, _ = ptypes.TimestampProto(time.Date(int(*params.YearStart), 1, 1, 0, 0, 0, 0, time.UTC))
	} else {
		yearStart, _ = ptypes.TimestampProto(time.Date(1900, 1, 1, 0, 0, 0, 0, time.UTC))
	}

	if params.YearEnd != nil {
		yearEnd, _ = ptypes.TimestampProto(time.Date(int(*params.YearEnd), 1, 1, 0, 0, 0, 0, time.UTC))
	} else {
		yearEnd = ptypes.TimestampNow()
	}

	req := &pb.ListMoviesRequest{
		Limit:            params.Limit,
		Offset:           offset,
		Visibility:       onlyRented,
		Mpa:              mpa,
		Category:         category,
		ReleaseDateStart: yearStart,
		ReleaseDateEnd:   yearEnd,
	}

	resp, err := s.MoviesServiceClient.ListMovies(ctx, req)
	if err != nil {
		return nil, err
	}

	//не понимаю как сделать нормально без маршалинга-размаршалинга

	payloadMov := []*models.Movie{}

	bytes, err := json.Marshal(resp.Movies)

	err = json.Unmarshal(bytes, &payloadMov)

	payload := &models.ListMoviesResponse{
		PageNum:  resp.PageNum,
		PageSize: resp.PageSize,
		Movies:   payloadMov,
	}
	return payload, nil
}
