package service

import (
	pb "gwusermod/api"
)

type GatewayUserService struct {
	AuthServiceClient   pb.AuthServiceClient
	UserServiceClient   pb.UserServiceClient
	MoviesServiceClient pb.MovieServiceClient
}
