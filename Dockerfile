FROM golang:alpine as modules

ADD go.mod go.sum /m/
RUN cd /m && go mod download

FROM golang:latest as builder

COPY --from=modules /go/pkg /go/pkg

RUN mkdir -p /app
ADD . /app
WORKDIR /app
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 \
    go build -o main ./cmd/gateway-user-server

#FROM golang:alpine
#RUN mkdir /app
#ADD . /app/
#WORKDIR /app

FROM scratch
COPY --from=builder /app/main /app/main
WORKDIR /app

ENV AUTH_SERVICE_GRPC_ADDR=9091
ENV MOVIES_SERVICE_GRPC_ADDR=9092
#RUN go build -o main ./cmd/gateway-user-server/
CMD ["/app/main --host=0.0.0.0 --port=8081"]
EXPOSE 8081